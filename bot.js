require('dotenv').config();
const https = require('https');
const fs = require('fs');
const path = require('path');
const { URL } = require('url');
const { Telegraf } = require('telegraf');
const fileReport = require('./vt');

const DOWNLOAD_DIR = './downloads/';

const bot = new Telegraf(process.env.BOT_TOKEN);

// Command-listener section
bot.start((ctx) => {
  ctx.reply(`Welcome, @${ctx.message.from.username}!`);
});

bot.help((ctx) => ctx.reply('Send me a sticker!'));

// Text-listener section
bot.on('sticker', (ctx) => {
  ctx.reply('👍');
});

bot.on('document', async (ctx) => {
  try {
    const fileInfo = ctx.update.message.document;
    const fileLinkInfo = await ctx.telegram.getFileLink(fileInfo.file_id);
    const fileLink = fileLinkInfo.href;

    const filePathLocal = await _downloadFile(fileLink);
    const filePathGlobal = path.resolve(__dirname, filePathLocal);

    let fileSizeString = `<i><b>🗂️ File size:</b> ${fileInfo.file_size} B</i>`;
    if (fileInfo.file_size > 1024 * 1024)
      fileSizeString = `<i><b>🗂️ File size:</b> ${Math.floor(fileInfo.file_size / (1024 * 1024), -1)} B</i>`;
    else if (fileInfo.file_size > 1024)
      fileSizeString = `<i><b>🗂️ File size:</b> ${Math.floor(fileInfo.file_size / 1024, -1)} B</i>`;

    ctx.reply(
      '<b><i>⏰ Your file is queued for analysis, usually it takes 2-3 minutes...</i></b>',
      { parse_mode: 'HTML' }
    );
    const data = await fileReport(filePathGlobal);
    // console.log(data);
    await ctx.reply(
      `<b>🔍 Detections: ${data.positives} / ${data.total}\n\n</b>` +
        `<i><b>🔖 File name:</b> ${fileInfo.file_name}</i>\n` +
        `<i><b>🏷️ File format:</b> ${filePathLocal
          .split('.')
          .pop()
          .toUpperCase()}</i>\n` +
        fileSizeString +
        `\n\n` +
        `<i><b>🔭 Scan date:</b>\n${data.scan_date}</i>`,
      { parse_mode: 'HTML' }
    );
  } catch (err) {
    console.log(err);
  }
});

bot.launch();

const _downloadFile = (url) => {
  return new Promise((resolve, reject) => {
    const reqUrl = new URL(url);
    const fileName = reqUrl.pathname.split('/').pop();
    const fileStream = fs.createWriteStream(DOWNLOAD_DIR + fileName);

    fileStream
      .on('open', () => {
        https.get(url, (res) => {
          res.pipe(fileStream);
        });
      })
      .on('error', () => {
        reject('Some troubles with writing of file...');
      })
      .on('finish', () => {
        resolve(fileStream.path);
      });
  });
};
