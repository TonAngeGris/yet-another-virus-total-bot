# Yet another [Virus Total](https://www.virustotal.com/) bot
A simple Telegram bot that can scan files on viruses using over [60 antiviruses](https://www.virustotal.com/), just send a file and wait a bit. That's simple!

### Requirements
- [Node.js](https://nodejs.org/) 13+
- npm 6+

### Installation
1. Clone the project
```sh
git clone https://gitlab.com/TonAngeGris/yet-another-virus-total-bot.git
cd yet-another-virus-total-bot/
```

2. Install dependencies:
```sh
npm i
```

### How to use
1. Put VirusTotal API key and Telegram Key inside `.env` in the root folder.
2. Run `npm start`.
3. Open telegram app and go to `@<link_to_your_bot>` you got from `@BotFather` and write `/start`.
