require('dotenv').config();
const fetch = require('node-fetch');
const { readFile } = require('fs/promises');
const FormData = require('form-data');
const { URL, URLSearchParams } = require('url');

const URL_SCAN_FILE = 'https://www.virustotal.com/vtapi/v2/file/scan';
const URL_REPORT_FILE = 'https://www.virustotal.com/vtapi/v2/file/report';
const API_KEY = process.env.API_KEY;

const fileReport = async (filePath) => {
  try {
    const resource = await _fileScan(filePath);

    const params = new URLSearchParams();
    params.append('apikey', API_KEY);
    params.append('resource', resource);

    const url = new URL(URL_REPORT_FILE);
    url.search = params.toString();

    let response = await fetch(url, { method: 'GET' });
    let data = await response.json();
    _logRequestInfo(data);

    if (data.response_code === Number(-2)) {
      console.log('code -2');
      return new Promise((resolve, reject) => {
        setInterval(async () => {
          response = await fetch(url, { method: 'GET' });
          data = await response.json();
          if (data.response_code === Number(1)) {
            // return data;
            resolve(data);
            // clearInterval(interval);
          }
          _logRequestInfo(data);
        }, 30000);
      });
      // let interval = setInterval(async () => {
      //   response = await fetch(url, { method: 'GET' });
      //   data = await response.json();
      //   if (data.response_code === Number(1)) {
      //     // return data;
      //     resolve(data);
      //     clearInterval(interval);
      //   }
      //   _logRequestInfo(data);
      // }, 30000);
    } else if (data.response_code === Number(1)) {
      return data;
    } else {
      return {
        ok: 'false',
        verbose_msg: 'Error with file uploading. Try to upload it again',
      };
    }
  } catch (err) {
    if (err instanceof fetch.FetchError) {
      return {
        ok: 'false',
        vebrose_msg:
          'Too many requests one-time, wait a couple of munites to try again',
      };
    }
    console.error(err);
  }
};

const _fileScan = async (filePath) => {
  try {
    const buffer = await readFile(filePath);
    const form = new FormData();
    form.append('apikey', API_KEY);
    form.append('file', buffer);

    const response = await fetch(URL_SCAN_FILE, { method: 'POST', body: form });
    const data = await response.json();
    return data.scan_id;
  } catch (err) {
    console.error(err);
  }
};

const _logRequestInfo = (data) => {
  if (data.response_code === Number(-2)) {
    console.log({
      'Response code': data.response_code,
      'Verbose mesage': data.verbose_msg,
      'Scan ID': data.scan_id,
    });
  } else if (data.response_code === Number(1)) {
    console.log({
      'Response code': data.response_code,
      'Verbose mesage': data.verbose_msg,
      'Permanent link': data.permalink,
      'Scan ID': data.scan_id,
    });
  }
};

module.exports = fileReport;
